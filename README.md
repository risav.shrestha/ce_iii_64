# CE_iii_64

Implementation of linear and binary search algorithms.



Generate some random inputs for your program and applying both linear
and binary search algorithms to find a particular element on the generated
input. 

Record the execution times of both algorithms for best and worst
cases on inputs of different size

The unit Test are implemented in the unitTests.py File

The graph is plotted in the Plot.py file
The search algorithms are implemented in the searchAlgorithm.py File





