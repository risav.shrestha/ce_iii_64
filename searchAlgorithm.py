array=[]

def LinearSearch(values,target):
    num=len(values)
    for j in range(num):
    	if values[j]==target:
        	return j
    return -1  

def BinarySearch(values,target):
	first = 0
	last=len(values)-1
	index= -1
	while first<=last and index==-1:
		midPoint = (first+last)//2
		if(values[midPoint] == target):
			index= midPoint
		else:
			if(values[midPoint]>target):
				last = midPoint-1
			else:
				first = midPoint+1
	return index

if __name__=="__main__":
	print("Enter the value to add to list and any other characters also")
	# For taking integer values 
	while True:
		try:
			x=input("Input Number:")
			x=int(x)
			array.append(x)
		except:
			break

	print(array)
	searchValue = int(input("Enter the values that must be searched from the lists:"))

	linearSearchValue = LinearSearch(array,searchValue)

	if(linearSearchValue==-1):
		print("The value",searchValue,"doesn't exist in the following list")
	else:
		print("The value lies in ",linearSearchValue,"of the Linear Search in sorted result")

	array.sort()
	binarySearchValue = BinarySearch(array,searchValue)

	if(binarySearchValue==-1):
		print("The value",searchValue,"doesn't exist in the following list")
	else:
		print("The value lies in ",binarySearchValue,"of the Binary Search in sorted result")

