from searchAlgorithm import LinearSearch
from searchAlgorithm import BinarySearch
import random
from time import time
import matplotlib.pyplot as plt

num=100000
linearTimeArray =[]
binaryTimeArray=[]
sizeArray =[]

# Linear Search
for j in range(num,num*11,num):
	sizeArray.append(j)
	randomvalues = random.sample(range(j), j)
	startTime = time()
	LinearSearch(randomvalues, randomvalues[j-1])
	endTime = time()
	totalTime = endTime - startTime
	linearTimeArray.append(totalTime)
	print("Linear search for the last value which is of size",j,"is",totalTime)

# Binary Search
for j in range(num,num*11,num):
	randomvalues = random.sample(range(j), j)
	startTime = time()
	randomvalues.sort()
	BinarySearch(randomvalues, randomvalues[j-1])
	endTime = time()
	totalTime = endTime - startTime
	binaryTimeArray.append(totalTime)
	print("Binary search for last value which is of size",j,"is",totalTime)

# Plot size vs time graph
fig, ax = plt.subplots(1, 1)
ax.plot(sizeArray,linearTimeArray, label = 'Linear Search')
ax.plot(sizeArray,binaryTimeArray, label = 'Binary Search')
legend = ax.legend(loc = 'upper center', fontsize = 'small')
plt.show()

