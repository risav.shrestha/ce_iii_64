import unittest
from searchAlgorithm import LinearSearch
from searchAlgorithm import BinarySearch

class SearchTestCase(unittest.TestCase):
# Linear Search Test
	def test_linear_search(self):
		array=[5,3,7,2,1,0,9,8,4]
		self.assertEqual(LinearSearch(array,5),0)	#5 lies in the 0th index so no error
		self.assertEqual(LinearSearch(array,1),4)
		self.assertEqual(LinearSearch(array,10),-1)

# Binary Search Test
	def test_binary_search(self):
		#sorted array for binary search
		array=[0,1,2,3,4,5,6,7,9]
		self.assertEqual(BinarySearch(array,5),5)
		self.assertEqual(BinarySearch(array,1),1)
		self.assertEqual(BinarySearch(array,10),-1)

if __name__ == '__main__':
	unittest.main()
